# This is a template for a Python program.

# Import the necessary modules.


## quest variables:
##
##

quest_adj = (
    "the",
    "a",
    "an"
    )

quest_noun = (
  "quest",
  "adventure",
  "journey",
  "expedition",
  "task",
  "campaign",
  "jaunt",
  "hike",
  "odyssey",
  "passage",
  "pilgrimage",
  "tour",
  "cruise",
  "safari",
  "crusade",
  "excursion",
  "search",
  "escort",
  )

quest_verb = (
  "defend",
  "find",
  "seek",
  "retrieve",
  "investigate",
  "gather",
  "weave",
  "collect",
  "rescue",
  "tend",
  "fight",
  "deliver",
  "learn",
  "serve",
  "invent",
  "attend",
  "join",
  "locate",
  "tame",
  "unleash",
  "train",
  "clear",
  "defeat",
  "repair",
  "obtain",
  "repair",
  "retake",
  "destroy",
  "forage",
  )

quest_object = (
  "treasure",
  "paladium vest",
  "artifact",
  "bowtie",
  "fez",
  "can of hooch",
  "random task",
  "livestock",
  "treehouse",
  "jury duty",
  "plague",
  "hanging",
  "lynch mob",
  "beast",
  "yolo",
  "parchment of gratitude",
  "sports",
  )

quest_modifier = (
  "lost",
  "mysterious",
  "dangerous",
  "haunted",
  "rare",
  "ancient",
  "family heirloom",
  "magical",
  "skeptical",
  "fantastical",
  "just plain weird",
  )
############

## not used ##

quest_list =(
  "find the artifact",
  "retrieve treasure",
  "invesitage old house",
  "rescue villagers",
  "collect random things",
  "spin wool",
  "weave cloth",
  "sew clothes",
  "make candles",
  "make soap",
  "wash dishes",
  "chicken care",
  "work in the fields",
  "tend the livestock",
  "gather firewood",
  "fetch water",
  "repair treehouses",
  "make tools",
  "farm veggies",
  "trade with natives",
  "fight unnamed war",
  "self care time",
  "celebrate holiday",
  "write letters",
  "learn something new",
  "serve jury duty",
  "serve court notice",
  "go to the market",
  "haggle with merchants",
  "pay taxes",
  "cure the plague",
  "invent item",
  "get robbed",
  "attend a hanging",
  "join a lynch mob",
  "attend pirate skool",
  "collect needed items",
  "tame the beast",
  "unleash the beast",
  "train in sports",
  "clear goblin cave",
  ## "defeat the " + mob_class(random.randrange(len(mob_class))),
  "yolo",
  "save yourself",
  "the journey",
  "the adventure",
  "the rescue",
  "the betrayal",
  "the redemption",
  "the legend",
  "the prophecy",
  "the end",
)

## end not used ##



## main program
