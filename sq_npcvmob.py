def npcvmob():
# larger ferocity goes first
# to hit enemy, d20 > toughness
# miss = focus < attack roll
# damage inflicted = (weapon strength * npc strength / mob's toughness)
# weapon toughness reduces until 0 (weapon broke, swap)
# vitality is reduced by damage inflicted
  global attackcnt
  global hitcnt
  #npc_log.write("enter npcvmob\n")
  #npc_log.write(str(npc_stats) + "\n")
  hitcnt = 0
  while (npc_stats["vitality"] > 0) and (mob_stats["vitality"] > 0):
    npcui()
    stdscr.addstr(23,0, "npc vitality: "+ str(npc_stats["vitality"]).rjust(4," "))
    stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
    stdscr.addstr(23,40, "attack count: " + str(attackcnt).rjust(2," "))
    stdscr.addstr(23,60, "hit count: " + str(hitcnt).rjust(2," "))
    stdscr.refresh()
    if npc_stats["ferocity"] > mob_stats["ferocity"]:
# npc attacks first
      attack_roll = roll_dice("d20")
      if (attack_roll + npc_stats["focus"]) > (mob_stats["toughness"] + mob_stats["focus"]):
        sqnnline=""
        sqnnline=[npc_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), mob_stats["name"], (" with " + weapon_stats["name"][0:19])]
        sqnnui(sqnnline)
        #npc_log.write(npc_stats["name"] + action_list[random.randrange(len(action_list))] + " with " + weapon_stats["name"])
        weapon_stats["toughness"] -= .25
        if weapon_stats["toughness"] <= 0:
          rollweapon()
        mob_stats["vitality"] -= int(weapon_stats["strength"] * npc_stats["strength"] / mob_stats["toughness"])
        stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
        stdscr.refresh() 
      else:
        sqnnline=""
        sqnnline=[npc_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
        sqnnui(sqnnline)
# mob attacks second
        attack_roll = roll_dice("d20")
        r=roll_dice("d4")
        for w in range(r):
          time.sleep(timeless * w)
        if (attack_roll + mob_stats["focus"]) > (npc_stats["toughness"] + npc_stats["focus"]):
          sqnnline=""
          sqnnline=[mob_stats["name"],((action_list[random.randrange(len(action_list))])[0:19]), npc_stats["name"], (" with " + (weapons_list[random.randrange(len(weapons_list))])[0:19])]
          sqnnui(sqnnline)
          npc_stats["vitality"] -= int(roll_dice("d12") * mob_stats["strength"] / (npc_stats["toughness"])) + (npc_stats["healing"])
          stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
          stdscr.refresh()
        else:
          sqnnline=""
          sqnnline=[mob_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
          sqnnui(sqnnline)
    else:
# mob attacks first
      attack_roll = roll_dice("d20")
      if (attack_roll + mob_stats["focus"]) > (npc_stats["toughness"] + npc_stats["focus"]):
        sqnnline=""
        sqnnline=[mob_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), npc_stats["name"], (" with " + (weapons_list[random.randrange(len(weapons_list))])[0:19])]
        sqnnui(sqnnline)
        npc_stats["vitality"] -= int((roll_dice("d12") * mob_stats["strength"] / npc_stats["toughness"])) + (npc_stats["healing"]/2)
        stdscr.addstr(23,0, "npc vitality: "+ str(npc_stats["vitality"]).rjust(4," "))
        stdscr.refresh()
        #npc_log.write("npc vitality change\n")
        #npc_log.write(str(npc_stats) + "\n")
        npc_stats["vitality"] += (npc_stats["healing"]/2)
      else:
        sqnnline=""
        sqnnline=[mob_stats["name"], (str(flight_list[random.randrange(len(flight_list))])), (preposition_list[random.randrange(len(preposition_list))]), (npc_stats["name"] + (" with " + str(weapons_list[random.randrange(len(weapons_list))])))]
        sqnnui(sqnnline)

# npc attacks second
      attack_roll = roll_dice("d20")
      if (attack_roll + npc_stats["focus"]) > (mob_stats["toughness"] + mob_stats["focus"]):
        sqnnline=""
        sqnnline=[npc_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), mob_stats["name"],(" with " + (weapon_stats["name"])[0:19])]
        sqnnui(sqnnline)
        #npc_log.write(npc_stats["name"] + " hits\n")
        r=roll_dice("d4")
        for w in range(r):
          time.sleep(timeless * w)
        weapon_stats["toughness"] -= .25
        if weapon_stats["toughness"] <= 0:
          rollweapon()
        mob_stats["vitality"] -= int((weapon_stats["strength"] * npc_stats["strength"] / mob_stats["toughness"]))
        stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
        stdscr.refresh()
        #npc_log.write("mob vitality change\n")
        #npc_log.write(str(mob_stats) + "\n")
      else:
        sqnnline=""
        sqnnline=[npc_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
        sqnnui(sqnnline)
    if npc_stats["vitality"] <= 0:
      npc_stats["vitality"] == (npc_stats["_vitality"] + npc_stats["healing"])
      sqnnline=""
      sqnnline=[npc_stats["name"], "gets an", "adrenaline boost"]
      time.sleep(timeless)
      sqnnui(sqnnline)
    hitcnt += 1
    stdscr.addstr(23,60, "hit count: " + str(hitcnt).rjust(2," "))
    stdscr.refresh()
    #npc_log.write("npcvmob hitcount: " + str(hitcnt) + "\n")
    ## npcui()

  if npc_stats["vitality"] <= 0:
    sqnnline=""
    sqnnline=[mob_stats["name"], "defeats", npc_stats["name"]]
    sqnnui(sqnnline)
    #npc_log.write(npc_stats["name"] + " lost\n")
    #npc_log.write(str(npc_stats) + "\n")
  elif mob_stats["vitality"] <= 0:
    sqnnline=""
    sqnnline=[npc_stats["name"], "defeats", mob_stats["name"]]
    sqnnui(sqnnline)
    #npc_log.write(mob_stats["name"] + " lost\n") 
    #npc_log.write(str(mob_stats) + "\n")
    attackcnt += 1
  time.sleep(timeless)
  #npc_log.write("npcvmob hitcount: " + str(hitcnt) + "\n")
  #npc_log.write("npcvmob attack count: " + str(attackcnt) + "\n")
  stdscr.addstr(23,40, "attack count: " + str(attackcnt).rjust(2," "))
  stdscr.refresh()
  hitcnt=0
  time.sleep(timeless)

  ## xp error here I think
  #npc_log.write("enter dealxp --  xp error\n")
  dealxp()
  #npc_log.write("exit dealxp --  xp error\n")
  ## end here

  ## time.sleep(timeless)
  getloot()
  time.sleep(timeless)
  #npc_log.write("exit npcvmob\n")
  return