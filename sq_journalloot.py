def getloot():
  global npc_loot
#  if mob_stats["level"] > 0:
#    nextloop = mob_stats["level"]
#  else:
  nextloop = roll_dice("d4")
  sqnnline=[npc_stats["name"], "rummages for loot"]
  sqnnui(sqnnline)
#  stdscr.addstr(20,25, "npc_loot before: " + str(len(npc_loot)).rjust(4," "))
#  stdscr.refresh()
  time.sleep(timeless)
  for lootloop in range(nextloop):
    npc_loot.append(loot_list[random.randrange(len(loot_list))])
    #npc_log.write("lootloop: " + str(lootloop) + "\n")
    lootui()
    time.sleep(timeless * roll_dice("d4"))
#  stdscr.addstr(21,25, "npc_loot after: " + str(len(npc_loot)).rjust(4," "))
#  stdscr.refresh()
  time.sleep(timeless)
  #npc_log.write("leave getloot\n")
  return

def draftjournal():
  global npc_journal
  
  if len(npc_journal) >= 20:
    journal2tome()

#  quest_name = adjectives[random.randint(0, len(adjectives) - 1)] + \
#  nouns[random.randint(0, len(nouns) - 1)] + \
#  verbs[random.randint(0, len(verbs) - 1)] + \
#  objects[random.randint(0, len(objects) - 1)]
  
  quest_name = ""
  quest_name += random.choice(quest_adj) + " "
  quest_name += random.choice(quest_noun) + " to "
  quest_name += random.choice(quest_verb) + " the "
  quest_name += random.choice(quest_modifier) + " "
  quest_name += random.choice(quest_object) + " "

  npc_journal.append(quest_name)
  npc_log.write("journal entry: " + str(quest_name)+ "\n")
  journalui()

  # quest_list[random.randrange(len(quest_list))]

def journal2tome():
  global npc_journal
  sqnnline=""
  sqnnline=""
  tmpbar=""
  sqnnline=["archiving journal entries", ""]
  sqnnui(sqnnline)
  time.sleep(timeless)
  npc_journal.insert(0, "".ljust(50," "))
  winrow=0
  while len(npc_journal) > 0:
    journal_pad.refresh(winrow,0,2,22,16,45)
    winrow += 1
    sqnnline=["archiving ", npc_journal[0][0:18], " to tome"]
    sqnnui(sqnnline)
    npc_journal.remove(npc_journal[0])
    journalui()
    tmpbar=progressbar(len(npc_journal), 30, 1, 28)
    time.sleep(timeless)
  journal_pad.clear()
  journalui()
  sqnnline=["finished archiving", ""]
  sqnnui(sqnnline)
  time.sleep(timeless)
  return

def tradingpost():
  global npc_loot
  #npc_log.write("enter trading post\n")
  sqnnline=""
  tmpbar=""
  sqnnline=["entering the", "trading post"]
  sqnnui(sqnnline)
  time.sleep(timeless)
  npc_loot.append("".ljust(30," "))
  winrow=0
  while len(npc_loot) > 0:
    loot_pad.refresh(winrow,0,2,22,16,45)
    winrow += 1
    if npc_stats["level"] < 10:
      coinlevel="iron"
    else:
      coinlevel="bronze"
      pass
    tmpbar=progressbar(len(npc_loot), 30, 1, 28)
    loot_winf.insstr(0,0,tmpbar)
    loot_winf.refresh()
    npc_coins[coinlevel] += 1
    for c in npc_coins.keys():
      while (npc_coins[c]-100) > 0:
        tmp = c + ": " + str(npc_coins[c])
        npc_coins[c] -= 100
        if c=="iron":
          npc_coins["bronze"] +=1
        elif c=="bronze":
          npc_coins["electrum"] +=1
        elif c=="electrum":
          npc_coins["paladium"] +=1
        elif c=="paladium":
          pass
          #npc_coins["crypto"] +=1 
        elif c=="cyrpto":
          pass
      coinbagui()
      time.sleep(timeless)
    sqnnline=["trading", npc_loot[0][0:18], "for a " + coinlevel + " coin"]
    sqnnui(sqnnline)
    npc_loot.remove(npc_loot[0])
  loot_pad.clear()
  lootui()
  sqnnline=["leaving the", "trading post"]
  sqnnui(sqnnline)
  time.sleep(timeless)
  return