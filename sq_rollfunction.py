# import modules
import time
import random
random.seed(time.time())

# define dice roll function

def roll_dice(dice_type) -> int:
  #dictionary
  dice_sides = {"d4": 4,
                "d6": 6,
                "d8": 8,
                "d10": 10,
                "d12": 12,
                "d20": 20,
                "d100": 100
                }
  return random.randint(1, dice_sides[dice_type])


  #npc_log.write("roll npc\n" + str(npc_stats))

def notused():
  npcui()
  for coin in npc_coins.keys():
    npc_coins[coin] = 0
  coinbagui()
  coin_win.refresh()
  tmpbar=progressbar(0,2000,100,20)
  npc_win.insstr(13,0,tmpbar)
  npc_win.refresh()


def rollweapon():
  for k in weapon_stats.keys():
    if k != "name":
      weapon_stats[k] = 0
      weapon_stats[k] = roll_dice("d6") + roll_dice("d6")
    else:
      weapon_stats[k] = weapons_list[random.randrange(len(weapons_list))]
