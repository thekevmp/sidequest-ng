# This is a template for a Python program.
# Import the necessary modules.
import time
import random
from dataclasses import dataclass, field
from sq_rollfunction import *
random.seed(time.time())


@dataclass
class Constants:
    ## dictionary
    biome = {"forest": "", "desert": "", "river": "", "lake": "",
            "mountains": "", "swamp": "", "taco truck": "", "town": "",
            "banlieu": "", "barrio": "", "campsite": "", "sewer": "", "back alley": ""
            }
    ## tuple for immutable
    coins_list = ("iron", "bronze", "electrum", "paladium", "crypto")

    ## tuple for immutable
    loot_list = ("titanium brick", "warm apple pie", "lighter", "toothpaste", "dental floss",
                 "sticky notes", "invitation", "bag o weed", "hooka", "bowtie", "fez", 
                 "guitar case full of guns", "ice cream cone", "autographed drumstick",
                 "cool sunglasses", "bitchin keychain", "an urn", "little black book",
                 "cool high five", "rock", "one red shoe", "book of hamunaptra", "parchment",
                 "single nostril sparrow's map", "old family numbers book", "rare painting",
                 "rubber duck", "personal communication device", "stick of gum", "rotary telephone",
                 "chakram", "energy stones", "japanese peace lily", "key to the 'bangler",
                 "jug of cucumber water", "procrastinatium ore", "lazyrilium ore", "chips & salsa",
                 "neodymium", "granny panties", "wood", "kevlar", "electrum eye", "ghatto", "tortoise's shell"
                )
