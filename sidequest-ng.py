# Import the necessary modules.
import os
import sys
import random
import time
import curses
from curses import wrapper
from curses.textpad import Textbox, rectangle
from sq_quests import *
from sq_npc_variables import *
from sq_mob_variables import *
from sq_weapons_coins_loot_variables import *
from sq_combat_variables import *
from sq_rollfunction import *
from sq_progressbar import progressbar
from sq_dealxp import dealxp
from sq_journalloot import *
from sq_npcvmob import npcvmob
from sq_npcvmob_new import npcvmob_new

random.seed(time.time())

def go(stdscr):
  
  ## ui setup
  uisetup(stdscr)
  lootui()
  coinbagui()
  journalui()
  time.sleep(timeless)
  rollnpc()
  time.sleep(timeless)
  rollweapon()
  time.sleep(timeless)
  rollmob()
  time.sleep(timeless)

  ## let the journey begin!
  for mobcnt in range(1,3500):
    draftjournal()
    journalui()
    mobperquest = 0
    for x in range(4):
       mobperquest += roll_dice("d4")
    for questcnt in range(mobperquest):
      rollmob()
      npcvmob()
      if len(npc_loot) >= lootmax:
        tradingpost()

def launch():
  os.system('clear')
  # Call the main function.

  @dataclass
  class BTS:
    #stdscr
    timeless: int = .025

  npc_log = open("sidequest.log", "w")
  npc_log.write("sidequest start\n")
  attackcnt=0
  npc=NpcStats()
  npc_deux=NpcStats(name="el malitol")
  mob=MobStats(name="Mobguy")
  mob_deux=MobStats()
#  mob_tres=MobStats()
  
  bts = BTS()
  loot_xp = Loot_XP()

  npcvmob_new()

#  wrapper(go)

  npc_log.write("sidequest end\n")
  npc_log.close()
  #stdscr.getch()

if __name__ == "__main__":
  launch()  