# This is a template for a Python program.

# Import the necessary modules.
import time
import random
from dataclasses import dataclass, field
from sq_rollfunction import *
random.seed(time.time())


@dataclass
class Combat:
    action_list =(
      "attacks",
      "bonks",
      "kicks",
      "charges",
      "shemps",
      "strangles",
      "grapples",
      "hits",
      "insults",
      "nutshots",
      "s.i.n.g.s",
      "double taps",
      "abootheid",
      "awa wi yes",
      "argy-bargys",
      "argy-barges",
      "belts",
      "blatters",
      "braws",
      "brawls",
      "clatters",
      "clouts",
      "ding-dongs",
      "duffs up",
      "fisticuffs",
      "gives skelp to",
      "goes at it",
      "has a go",
      "has a scrap",
      "knuckles down",
      "lumps",
      "munts",
      "punches up",
      "rows",
      "scraps",
      "sets about",
      "sets to",
      "skelps",
      "stoushes",
      "thumps",
      "wallops",
      "whacks",
      "whams",
      "whops",
      "arvo bashes",
      "biffs",
      "blues",
      "boxes on",
      "clatters",
      "duffs up",
      "fair dinkums",
      "has a go",
      "king hits",
      "knocks around",
      "knocks someone around",
      "knuckles up",
      "munts",
      "punches on",
      "puts the boots to",
      "rolls around",
      "scraps",
      "stoushs",
      "throws down",
      "thumps",
      "wallops",
      "whacks",
      "whams",
      "whops",
      "yardie rumbles",
      "ahabs",
      "ballyrags",
      "belts",
      "biffs",
      "bludges",
      "boshes",
      "boxes",
      "cans",
      "clatters",
      "clogs",
      "derrys",
      "dosses",
      "dozes",
      "eejjits",
      "fecks",
      "fights",
      "floors",
      "gobs",
      "headbutts",
      "corn holes",
      "kicks",
      "knocks",
      "lumps",
      "mashes",
      "mucks",
      "pads",
      "punches",
      "slaps",
      "slogs",
      "smacks",
      "soaks",
      "thumps",
      "wallops",
      "whacks",
      "whams",
      "whoops",
      "yanks",
      "has a go at",
      "has a pop at",
      "lays into",
      "lets rip at",
      "mounts an attack on",
      "nips at",
      "takes a swing at",
      "tees off on",
      "tears into",
      "whacks at",
      "bashes",
      "beats",
      "batters",
      "belts",
      "clobbers",
      "decks",
      "dusts up",
      "flattens",
      "floors",
      "gives someone a hiding",
      "gives someone a pasting",
      "pummels",
      "roughs up",
      "slugs",
      "smacks",
      "takes down",
      "trounces",
      "wallops",
    )

    preposition_list =(
      "about",
      "above",
      "across",
      "after",
      "against",
      "along",
      "among",
      "around",
      "at",
      "before",
      "behind",
      "below",
      "beside",
      "between",
      "by",
      "concerning",
      "despite",
      "during",
      "except",
      "for",
      "from",
      "in",
      "inside",
      "into",
      "like",
      "near",
      "of",
      "on",
      "onto",
      "out",
      "outside",
      "over",
      "past",
      "through",
      "to",
      "toward",
      "under",
      "underneath",
      "until",
      "up",
      "upon",
      "with",
      "within",
      "without",
    )

    flight_list =(
      "trips",
      "falls down",
      "is winded",
      "poops",
      "ties shoe",
      "stabs his own eye",
      "tells a joke",
      "mimes a wall",
      "yells lookout!",
      "feigns death",
      "dances",
      "monologues",
      "bumbles",
      "fumbles",
      "dives",
      "flops",
      "stumbles",
      "trips",
      "wobbles",
      "slips",
      "skids",
      "crashes",
      "blunders",
      "botchs",
      "bungles",
      "messes up",
      "flunks",
      "fails",
      "bombs",
      "flops",
      "blows it",
      "comes up short",
      "falls short",
      "comes to grief",
      "comes a cropper",
      "bites the dust",
      "goes down in flames",
      "buggered it up",
      "blew it",
      "fell flat on my face",
      "crapped out",
      "duff it up",
      "flumped it",
      "stank it up",
      "took a tumble",
      "went belly up",
      "went tits up",
    )

