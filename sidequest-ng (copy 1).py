# Import the necessary modules.
import os
import sys
import random
import time
from sq_quests import *
from sq_npc_variables import *
from sq_mob_variables import *
from sq_weapons_coins_loot_variables import *
from sq_combat_variables import *
import curses
from curses import wrapper
from curses.textpad import Textbox, rectangle
random.seed(time.time())

def uisetup(tmpscr):
  global npc_win
  global loot_winh
  global loot_winf
  global coin_win
  global sqnn_win
  global journal_winh
  global loot_pad
  global journal_pad
  global stdscr
  stdscr = tmpscr

  #<window> = curses.newwin(h, w, r, c)
  #<pad> = curses.newpad(h,w)
  #<pad>.refresh(pr, pc, r, c, h, w)
  #<pad>.refresh(pad content-r, pad content-c, display screen row, display screen column, height, width)
  #start pad at cr, cc.
  
  stdscr.clear()
  rectangle(stdscr, 0, 0, 22, 79)       #main rectangle
  rectangle(stdscr, 0,0,15,21)          #npc stats
  rectangle(stdscr, 15,0,22,21)         #sidequest news network
  rectangle(stdscr, 0,21,18,50)         #loot
  rectangle(stdscr, 0,50,18,79)         #journal window
  stdscr.refresh()

  npc_win = curses.newwin(14, 20, 1, 1)   #npc stats window
  coin_win = curses.newwin(6, 20, 16 ,1)
  loot_winh = curses.newwin(1,28, 1, 22)
  loot_winf = curses.newwin(1,28, 17,22)
  journal_winh = curses.newwin(1,27,1,51)
  sqnn_win = curses.newwin(3, 56, 19, 22)

  loot_pad = curses.newpad(400,30)        #loot area
  journal_pad = curses.newpad(1200,27)

  npc_win.clear()
  coin_win.clear()
  loot_pad.clear()
  loot_winh.clear()
  loot_winf.clear()
  journal_winh.clear()
  journal_pad.clear()
  sqnn_win.clear()

  loot_winh.refresh()
  loot_winf.refresh()
  loot_pad.refresh(0,0,2,22,16,45)
  journal_pad.refresh(0,0,2,51,16,78)
  sqnn_win.refresh()

  time.sleep(timeless)

# define npc ui layout
def npcui():
  global xpneed
  global xphave
  global xplvl

  npc_win.refresh()
  time.sleep(timeless)
  npc_win.addstr(0,0, "npc:".center(20," "))
  winrow=1
  for k in npc_stats.keys():
    if k == "name" or k == "archetype":
      npc_win.addstr(winrow,0,(str(npc_stats[k])))
      npc_win.addstr(1,13,(str(npc_stats["level"]).rjust(7," ")))
      npc_win.refresh()
    elif k != "level" and k[0:1] != "_":
      if k == "xp":
        winrow +=0
      npc_win.addstr(winrow,0,(k + ": "))
      npc_win.addstr(winrow,13,(str(npc_stats[k]).rjust(7," ")))
      npc_win.refresh()
    winrow += 1
  #totalxp = str(npc_stats["xp"]) + "-" + str(xplvl)
  totalxp = str(npc_stats["xp"] - xphave) + "/" + str(xpneed)
  #totalxp = str(npc_stats["xp"] - xphave) + "/" + str(xpneed)
  npc_win.addstr(12,5,(totalxp.rjust(15," ")))  
  npc_win.refresh()
  time.sleep(timeless)

def sqnnui(displayitems):
  sqnn_win.clear()
  sqnn_win.addstr(0,0,"sidequest news network".center(58," "))
#  line=2
  sqnn_txt = ""
  for linestr in displayitems:
    sqnn_txt += str(linestr[0:18]) + " "
    #sqnn_win.addstr(line,0, str(linestr[0:18]))
#    line +=1
  sqnn_win.addstr(1,0,sqnn_txt)
  sqnn_win.refresh()
  return

def coinbagui():
  coin_win.clear()
  coin_win.addstr(0,0,"coin bag".center(20," "))
  line=1
  for coin in npc_coins.keys():
    coin_win.addstr(line,0, coin + ": ")
    coin_win.addstr(line, 11, str(npc_coins[coin]).rjust(8," "))
    line +=1
  coin_win.refresh()
  return

def lootui():
  tmpbar=""
  loot_winh.insstr(0,0,"loot:".center(28," "))
  loot_winh.refresh()
  winrow=0
  if len(npc_loot) > 16:
    winrow = (len(npc_loot) - 16)
  for lootcnt in range(len(npc_loot)):
    tmploot = npc_loot[lootcnt]
    tmploot = tmploot.ljust(30," ")
    loot_pad.addstr(lootcnt,0,tmploot[0:25])
    loot_pad.refresh(winrow,0,2,22,16,45)
    time.sleep(timeless)
  tmpbar=progressbar(len(npc_loot), 30, 1, 28)
  loot_winf.insstr(0,0,tmpbar)
  loot_winf.refresh()


def journalui():
  journal_winh.clear()
  journal_winh.insstr(0,7,"journal: ")
  journal_winh.refresh()
  #journal_pad.refresh(0,0,2,51,16,78)
  wrow=0
  for journalcnt in range(len(npc_journal)-1,-1,-1):
    if journalcnt != len(npc_journal)-1:
      journal_pad.addstr(wrow,0, "\u2713")
    tmpjournal = npc_journal[journalcnt]
    tmpjournal = tmpjournal.ljust(81," ")
    journal_pad.addstr(wrow,1, tmpjournal)
    wrow += 3
    journal_winh.addstr(0,17,(str(len(npc_journal)).rjust(4," ")))
    journal_pad.refresh(0,0,2,51,16,78)
    journal_winh.refresh()
  time.sleep(timeless)

# define dice roll function
def roll_dice(dice_type):
  return random.randint(1, dice_sides[dice_type])

# roll a npc
def rollnpc():
  for k in npc_stats.keys():
    if k != "name" and k != "archetype":
      unusedroll=roll_dice("d6")
      for roll in range(1,4):
        diceroll=roll_dice("d6")
        if diceroll >= unusedroll:
          npc_stats[k] = npc_stats[k] + diceroll
          unusedroll=unusedroll
        else:
          npc_stats[k] = npc_stats[k] + unusedroll
          unusedroll=diceroll
  npc_stats["archetype"] = npc_class[random.randrange(len(npc_class))]
  npc_stats["vitality"] *= roll_dice("d10")
  npc_stats["_vitality"] = npc_stats["vitality"]
  npc_stats["xp"] = 0
  npc_stats["level"] = 1
  #npc_log.write("roll npc\n" + str(npc_stats))
  npcui()
  for coin in npc_coins.keys():
    npc_coins[coin] = 0
  coinbagui()
  coin_win.refresh()
  tmpbar=progressbar(0,2000,100,20)
  npc_win.insstr(13,0,tmpbar)
  npc_win.refresh()

# roll a mob
def rollmob():
  for k in mob_stats.keys():
    if k != "name" and k != "archetype":
      mob_stats[k] = 0
      for roll in range(1,5):
        diceroll=roll_dice("d4")
        mob_stats[k] = mob_stats[k] + diceroll
    else:
      mob_stats["name"] = mob_names[random.randrange(len(mob_names))]
      mob_stats["archetype"] = mob_class[random.randrange(len(mob_class))]
  mob_stats["level"] = npc_stats["level"] + (random.randint(-1,1))
  mob_stats["vitality"] *= roll_dice("d8")
  mob_stats["vitality"] += (10*mob_stats["level"])
  mob_stats["_vitality"] = mob_stats["vitality"]
  #npc_log.write("roll mob\n" + str(mob_stats))

def rollweapon():
  for k in weapon_stats.keys():
    if k != "name":
      weapon_stats[k] = 0
      weapon_stats[k] = roll_dice("d6") + roll_dice("d6")
    else:
      weapon_stats[k] = weapons_list[random.randrange(len(weapons_list))]

# would like to convert dealxp to asyncio
def dealxp():
  # xp for next level = 1400 + (600 * npc_stats["level"])
  # npc_stats["xp"] += 100
  global xphave
  global xpneed
  global xphave
  global xplvl
  loop=1
  xphave=0  #xp to start level 
  xpneed=0  #xp needed to level
  xplvl=0   #xp to end level
  tmpbar=""  

  ## how much xp is earned
  for r in range(5):
    npc_stats["xp"] += roll_dice("d20")
  ##

  xpneed = int(1400 + (600 * npc_stats["level"]))
  for i in range(1,npc_stats["level"]):
    if i > 0:
      xphave += int(1400 + (600 * (i)))
  xpgain = int((xpneed)/20)
  xplvl = xpneed + xphave
  xpgain = int((xpneed)/20)
  xpdiff = int(xpneed-xphave)


  if npc_stats["xp"] >= xplvl:
    ## d i n g ##
    npc_stats["level"] += 1
    npc_stats["_vitality"] += 5

    ## journal arching to a tomb

    ## stats increase

    ## coin increase

    
##
##
  npc_stats["vitality"] = npc_stats["_vitality"]
  #xpgain = int((xpneed - xphave)/20)
  # npcui()
  time.sleep(timeless)

  tmpbar = progressbar((npc_stats["xp"] - xphave), xpneed, xpgain, 20)
  #tmpbar = progressbar(npc_stats["xp"], xplvl, (xpgain), 20)

  #npc_log.write(str(npc_stats) + "\n")
  #totalxp = str(npc_stats["xp"]) + "/" + str(xplvl)
  totalxp = str(npc_stats["xp"] - xphave) + "/" + str(xpneed)
  #totalxp = str(npc_stats["xp"]) + "/" + str(xplvl)
  npc_win.addstr(12,5,(totalxp.rjust(15," ")))  
  npc_win.insstr(13,0,tmpbar)
  npc_win.refresh()
  #npc_log.write("leave dealxp\n")
  time.sleep(timeless)


def progressbar(barstart, barend, step, barlen):
  barstr="".ljust(barlen,"-")
#  xpdiff = int((barlen-(barend-barstart)/step))
  block = int((barlen-(barend - barstart)/step))
  barstr = ("#" * block).ljust(barlen,"-")
#  stdscr.addstr(19,50, "xp-barstart: " + str(barstart).ljust(6," "))
#  stdscr.addstr(20,50, "xpgain-barend: "+ str(barend).ljust(6," "))
#  stdscr.addstr(21,50, "xpgain/20-step: "+ str(step).ljust(6," "))
#  stdscr.addstr(22,50, "block: " + str(block))
#  stdscr.refresh()
  time.sleep(timeless)
  return(barstr)

def getloot():
  global npc_loot
#  if mob_stats["level"] > 0:
#    nextloop = mob_stats["level"]
#  else:
  nextloop = roll_dice("d4")
  sqnnline=[npc_stats["name"], "rummages for loot"]
  sqnnui(sqnnline)
#  stdscr.addstr(20,25, "npc_loot before: " + str(len(npc_loot)).rjust(4," "))
#  stdscr.refresh()
  time.sleep(timeless)
  for lootloop in range(nextloop):
    npc_loot.append(loot_list[random.randrange(len(loot_list))])
    #npc_log.write("lootloop: " + str(lootloop) + "\n")
    lootui()
    time.sleep(timeless * roll_dice("d4"))
#  stdscr.addstr(21,25, "npc_loot after: " + str(len(npc_loot)).rjust(4," "))
#  stdscr.refresh()
  time.sleep(timeless)
  #npc_log.write("leave getloot\n")
  return

def draftjournal():
  global npc_journal
  
  if len(npc_journal) >= 20:
    journal2tome()

#  quest_name = adjectives[random.randint(0, len(adjectives) - 1)] + \
#  nouns[random.randint(0, len(nouns) - 1)] + \
#  verbs[random.randint(0, len(verbs) - 1)] + \
#  objects[random.randint(0, len(objects) - 1)]
  
  quest_name = ""
  quest_name += random.choice(quest_adj) + " "
  quest_name += random.choice(quest_noun) + " to "
  quest_name += random.choice(quest_verb) + " the "
  quest_name += random.choice(quest_modifier) + " "
  quest_name += random.choice(quest_object) + " "

  npc_journal.append(quest_name)
  npc_log.write("journal entry: " + str(quest_name)+ "\n")
  journalui()

  # quest_list[random.randrange(len(quest_list))]

def journal2tome():
  global npc_journal
  sqnnline=""
  sqnnline=""
  tmpbar=""
  sqnnline=["archiving journal entries", ""]
  sqnnui(sqnnline)
  time.sleep(timeless)
  npc_journal.insert(0, "".ljust(50," "))
  winrow=0
  while len(npc_journal) > 0:
    journal_pad.refresh(winrow,0,2,22,16,45)
    winrow += 1
    sqnnline=["archiving ", npc_journal[0][0:18], " to tome"]
    sqnnui(sqnnline)
    npc_journal.remove(npc_journal[0])
    journalui()
    tmpbar=progressbar(len(npc_journal), 30, 1, 28)
    time.sleep(timeless)
  journal_pad.clear()
  journalui()
  sqnnline=["finished archiving", ""]
  sqnnui(sqnnline)
  time.sleep(timeless)
  return

def tradingpost():
  global npc_loot
  #npc_log.write("enter trading post\n")
  sqnnline=""
  tmpbar=""
  sqnnline=["entering the", "trading post"]
  sqnnui(sqnnline)
  time.sleep(timeless)
  npc_loot.append("".ljust(30," "))
  winrow=0
  while len(npc_loot) > 0:
    loot_pad.refresh(winrow,0,2,22,16,45)
    winrow += 1
    if npc_stats["level"] < 10:
      coinlevel="iron"
    else:
      coinlevel="bronze"
      pass
    tmpbar=progressbar(len(npc_loot), 30, 1, 28)
    loot_winf.insstr(0,0,tmpbar)
    loot_winf.refresh()
    npc_coins[coinlevel] += 1
    for c in npc_coins.keys():
      while (npc_coins[c]-100) > 0:
        tmp = c + ": " + str(npc_coins[c])
        npc_coins[c] -= 100
        if c=="iron":
          npc_coins["bronze"] +=1
        elif c=="bronze":
          npc_coins["electrum"] +=1
        elif c=="electrum":
          npc_coins["paladium"] +=1
        elif c=="paladium":
          pass
          #npc_coins["crypto"] +=1 
        elif c=="cyrpto":
          pass
      coinbagui()
      time.sleep(timeless)
    sqnnline=["trading", npc_loot[0][0:18], "for a " + coinlevel + " coin"]
    sqnnui(sqnnline)
    npc_loot.remove(npc_loot[0])
  loot_pad.clear()
  lootui()
  sqnnline=["leaving the", "trading post"]
  sqnnui(sqnnline)
  time.sleep(timeless)
  return

def npcvmob():
# larger ferocity goes first
# to hit enemy, d20 > toughness
# miss = focus < attack roll
# damage inflicted = (weapon strength * npc strength / mob's toughness)
# weapon toughness reduces until 0 (weapon broke, swap)
# vitality is reduced by damage inflicted
  global attackcnt
  global hitcnt
  #npc_log.write("enter npcvmob\n")
  #npc_log.write(str(npc_stats) + "\n")
  hitcnt = 0
  while (npc_stats["vitality"] > 0) and (mob_stats["vitality"] > 0):
    npcui()
    stdscr.addstr(23,0, "npc vitality: "+ str(npc_stats["vitality"]).rjust(4," "))
    stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
    stdscr.addstr(23,40, "attack count: " + str(attackcnt).rjust(2," "))
    stdscr.addstr(23,60, "hit count: " + str(hitcnt).rjust(2," "))
    stdscr.refresh()
    if npc_stats["ferocity"] > mob_stats["ferocity"]:
# npc attacks first
      attack_roll = roll_dice("d20")
      if (attack_roll + npc_stats["focus"]) > (mob_stats["toughness"] + mob_stats["focus"]):
        sqnnline=""
        sqnnline=[npc_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), mob_stats["name"], (" with " + weapon_stats["name"][0:19])]
        sqnnui(sqnnline)
        #npc_log.write(npc_stats["name"] + action_list[random.randrange(len(action_list))] + " with " + weapon_stats["name"])
        weapon_stats["toughness"] -= .25
        if weapon_stats["toughness"] <= 0:
          rollweapon()
        mob_stats["vitality"] -= int(weapon_stats["strength"] * npc_stats["strength"] / mob_stats["toughness"])
        stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
        stdscr.refresh() 
      else:
        sqnnline=""
        sqnnline=[npc_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
        sqnnui(sqnnline)
# mob attacks second
        attack_roll = roll_dice("d20")
        r=roll_dice("d4")
        for w in range(r):
          time.sleep(timeless * w)
        if (attack_roll + mob_stats["focus"]) > (npc_stats["toughness"] + npc_stats["focus"]):
          sqnnline=""
          sqnnline=[mob_stats["name"],((action_list[random.randrange(len(action_list))])[0:19]), npc_stats["name"], (" with " + (weapons_list[random.randrange(len(weapons_list))])[0:19])]
          sqnnui(sqnnline)
          npc_stats["vitality"] -= int(roll_dice("d12") * mob_stats["strength"] / (npc_stats["toughness"])) + (npc_stats["healing"])
          stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
          stdscr.refresh()
        else:
          sqnnline=""
          sqnnline=[mob_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
          sqnnui(sqnnline)
    else:
# mob attacks first
      attack_roll = roll_dice("d20")
      if (attack_roll + mob_stats["focus"]) > (npc_stats["toughness"] + npc_stats["focus"]):
        sqnnline=""
        sqnnline=[mob_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), npc_stats["name"], (" with " + (weapons_list[random.randrange(len(weapons_list))])[0:19])]
        sqnnui(sqnnline)
        npc_stats["vitality"] -= int((roll_dice("d12") * mob_stats["strength"] / npc_stats["toughness"])) + (npc_stats["healing"]/2)
        stdscr.addstr(23,0, "npc vitality: "+ str(npc_stats["vitality"]).rjust(4," "))
        stdscr.refresh()
        #npc_log.write("npc vitality change\n")
        #npc_log.write(str(npc_stats) + "\n")
        npc_stats["vitality"] += (npc_stats["healing"]/2)
      else:
        sqnnline=""
        sqnnline=[mob_stats["name"], (str(flight_list[random.randrange(len(flight_list))])), (preposition_list[random.randrange(len(preposition_list))]), (npc_stats["name"] + (" with " + str(weapons_list[random.randrange(len(weapons_list))])))]
        sqnnui(sqnnline)

# npc attacks second
      attack_roll = roll_dice("d20")
      if (attack_roll + npc_stats["focus"]) > (mob_stats["toughness"] + mob_stats["focus"]):
        sqnnline=""
        sqnnline=[npc_stats["name"], ((action_list[random.randrange(len(action_list))])[0:19]), mob_stats["name"],(" with " + (weapon_stats["name"])[0:19])]
        sqnnui(sqnnline)
        #npc_log.write(npc_stats["name"] + " hits\n")
        r=roll_dice("d4")
        for w in range(r):
          time.sleep(timeless * w)
        weapon_stats["toughness"] -= .25
        if weapon_stats["toughness"] <= 0:
          rollweapon()
        mob_stats["vitality"] -= int((weapon_stats["strength"] * npc_stats["strength"] / mob_stats["toughness"]))
        stdscr.addstr(23,20, "mob vitality: "+ str(mob_stats["vitality"]).rjust(4," "))
        stdscr.refresh()
        #npc_log.write("mob vitality change\n")
        #npc_log.write(str(mob_stats) + "\n")
      else:
        sqnnline=""
        sqnnline=[npc_stats["name"], ((flight_list[random.randrange(len(flight_list))])[0:19]), (preposition_list[random.randrange(len(preposition_list))]), (" " + mob_stats["name"])]
        sqnnui(sqnnline)
    if npc_stats["vitality"] <= 0:
      npc_stats["vitality"] == (npc_stats["_vitality"] + npc_stats["healing"])
      sqnnline=""
      sqnnline=[npc_stats["name"], "gets an", "adrenaline boost"]
      time.sleep(timeless)
      sqnnui(sqnnline)
    hitcnt += 1
    stdscr.addstr(23,60, "hit count: " + str(hitcnt).rjust(2," "))
    stdscr.refresh()
    #npc_log.write("npcvmob hitcount: " + str(hitcnt) + "\n")
    ## npcui()

  if npc_stats["vitality"] <= 0:
    sqnnline=""
    sqnnline=[mob_stats["name"], "defeats", npc_stats["name"]]
    sqnnui(sqnnline)
    #npc_log.write(npc_stats["name"] + " lost\n")
    #npc_log.write(str(npc_stats) + "\n")
  elif mob_stats["vitality"] <= 0:
    sqnnline=""
    sqnnline=[npc_stats["name"], "defeats", mob_stats["name"]]
    sqnnui(sqnnline)
    #npc_log.write(mob_stats["name"] + " lost\n") 
    #npc_log.write(str(mob_stats) + "\n")
    attackcnt += 1
  time.sleep(timeless)
  #npc_log.write("npcvmob hitcount: " + str(hitcnt) + "\n")
  #npc_log.write("npcvmob attack count: " + str(attackcnt) + "\n")
  stdscr.addstr(23,40, "attack count: " + str(attackcnt).rjust(2," "))
  stdscr.refresh()
  hitcnt=0
  time.sleep(timeless)

  ## xp error here I think
  #npc_log.write("enter dealxp --  xp error\n")
  dealxp()
  #npc_log.write("exit dealxp --  xp error\n")
  ## end here

  ## time.sleep(timeless)
  getloot()
  time.sleep(timeless)
  #npc_log.write("exit npcvmob\n")
  return

def go(stdscr):
  
  ## ui setup
  uisetup(stdscr)
  lootui()
  coinbagui()
  journalui()
  time.sleep(timeless)
  rollnpc()
  time.sleep(timeless)
  rollweapon()
  time.sleep(timeless)
  rollmob()
  time.sleep(timeless)

  ## let the journey begin!
  for mobcnt in range(1,3500):
    draftjournal()
    journalui()
    mobperquest = 0
    for x in range(4):
       mobperquest += roll_dice("d4")
    for questcnt in range(mobperquest):
      rollmob()
      npcvmob()
      if len(npc_loot) > lootmax:
        tradingpost()

def launch():
  os.system('clear')
  # Call the main function.
  global stdscr
  global timeless
  global xpneed
  global xphave
  global xplvl
  global lootmax
  

  @dataclass
  class Loot_XP:
    xpneed: int = 0
    xphave: int = 0
    xplvl: int = 0
    lootmax: int = 30
  
  loot_xp = Loot_XP()
  
  xpneed = 0
  xphave = 0
  xplvl = 0
  lootmax=30
  
  timeless = .025
  npc_log = open("sidequest.log", "w")
  npc_log.write("sidequest start\n")
  attackcnt=0
  wrapper(go)
  npc_log.write("sidequest end\n")
  npc_log.close()
  stdscr.getch()

if __name__ == "__main__":
  launch()  