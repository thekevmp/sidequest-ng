# This is a template for a Python program.

# Import the necessary modules.
import time
import random
from dataclasses import dataclass, field
from sq_rollfunction import *
random.seed(time.time())

# roll a mob
def rollmob():
  totalrolls = 0
  dice_rolls = [roll_dice("d4") for _ in range(6)]
  totalrolls += sum(max(dice_roll, 0) for dice_roll in dice_rolls)
  return totalrolls
  
def notused():
  for k in mob_stats.keys():
#    if k != "name" and k != "archetype:
      mob_stats[k] = 0
      for roll in range(1,5):
        diceroll=roll_dice("d4")
        mob_stats[k] = mob_stats[k] + diceroll
#    else:
      mob_stats["name"] = mob_names[random.randrange(len(mob_names))]
      mob_stats["archetype"] = mob_class[random.randrange(len(mob_class))]
  mob_stats["level"] = npc_stats["level"] + (random.randint(-1,1))
  mob_stats["vitality"] *= roll_dice("d8")
  mob_stats["vitality"] += (10*mob_stats["level"])
  mob_stats["_vitality"] = mob_stats["vitality"]
  #npc_log.write("roll mob\n" + str(mob_stats))

@dataclass
class MobStats:
    name: str = field(default_factory=lambda: random.choice(MobType.name))
    archetype: str = field(default_factory=lambda: random.choice(MobType.archetype))
    strength: int = field(default_factory=rollmob)
    precision:int = field(default_factory=rollmob)
    ferocity: int = field(default_factory=rollmob)
    damage: int = field(default_factory=rollmob)
    focus: int = field(default_factory=rollmob)
    healing: int = field(default_factory=rollmob)
    toughness: int = field(default_factory=rollmob)
    vitality: int = field(default_factory=rollmob)
    _vitality: int = field(init=False)
    xp: int = 0
    level: int = 0
    weapon: dict = field(default_factory=lambda: {
          "name": random.choice(MobType.weaponlist), 
          "strength": (roll_dice("d12") + roll_dice("d8")),
          "precision": (roll_dice("d12") + roll_dice("d8")),
          "damage": (roll_dice("d12") + roll_dice("d8")),
          "toughness": (roll_dice("d12") + roll_dice("d8"))
    } )


    def __post_init__(self):
      self._vitality = self.vitality


@dataclass
class MobType:
    archetype: tuple =("goblin", "orc", "bald eagle", "curmugeon", "satyr", "minotaur", "cyclops",
                      "panhandler", "vagrant", "whipper snapper", "troll", "skeleton", "demon",
                      "shadow", "mummy", "harpy", "humanoid boar", "dude", "mecha lizard",
                      "giant gorilla", "centaur", "mutated turtle", "sock puppet"
    )

    name: tuple =("Slick", "Pal", "Champ", "Sport", "Buddy", "Smash", "Skull", "Razor", "Chopper", 
                  "Knuckles", "Thug", "Shorty", "Stretch", "Brute", "Butthead", "Goon", "Vicious",
                  "Bones", "Buster", "Crank", "Danger", "Digger", "Dread", "Fang", "Greaseball",
                  "Hammer", "Jaws", "Easy", "Fang", "Flash", "Gator", "Goon", "Greaseball", "Hawk",
                  "Heat", "Acid", "Aftershock", "Apocalypse", "Bane", "Bloodbath", "Chainsaw", 
                  "Cobra", "Death", "Demolition", "Disaster", "Snake", "Spike", "Spud", "Stump",
                  "Thug", "Twister", "Vicious", "Wade", "Wild Bill", "Wolf", "X-Ray", "Yankee",
                  "Zeek", "Razor", "Reaoer", "Psycho", "Meatball", "Madman", "Machine", "Twister",
                  "Salazar"
    )

    weaponlist: tuple=("a rock", "used toilet paper", "2x4 w/rusted nails", "crotch punch", "trident", 
                "used condom", "verbal insult", "slapjack", "shotgun", "marbles", "ping pong paddle",
                "potato", "knuckle busters", "razor wire", "bag of sugar", "used drug needle", "purse", 
                "flail", "cocaine fingernail", "broken bottle", "bag of sewage", "slingshot", "morning star"
    )


## main program
