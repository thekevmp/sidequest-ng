# This is a template for a Python program.

# Import the necessary modules.
import time
import random
from dataclasses import dataclass, field
from sq_rollfunction import *
random.seed(time.time())


#### Calculate stats using an inline approach
# List of dice rolls 

#### Calculate stats using an inline approach


# roll a npc
def rollnpc() -> int:
  totalrolls=0
  unused_roll=roll_dice("d6")
  dice_rolls = [roll_dice("d6") for _ in range(3)]
  totalrolls += sum(max(dice_roll, unused_roll) if dice_roll >= unused_roll else unused_roll for dice_roll in dice_rolls)
  return totalrolls

@dataclass
class NpcStats:
    name: str = "heigh row"
    archetype: str = field(default_factory=lambda: random.choice(NpcRole.archetype))
    strength: int = field(default_factory=rollnpc)
    precision: int = field(default_factory=rollnpc)
    ferocity: int = field(default_factory=rollnpc)
    damage: int = field(default_factory=rollnpc)
    focus: int = field(default_factory=rollnpc)
    healing: int = field(default_factory=rollnpc)
    toughness: int = field(default_factory=rollnpc)
#    vitality: int = field(default_factory=lambda: 10 * 5)
    vitality: int = field(default_factory=lambda: (rollnpc() * roll_dice("d10")))
    _vitality: int= field(init=False)
    xp: int = 0
    level: int = 1
    npc_loot ={}
    npc_journal =[]
    npc_coins: dict = field(default_factory=lambda: {"iron": 0,"bronze": 0,"electrum": 0,"paladium": 0,"crypto": 0})
    weapon: dict = field(default_factory=lambda: {
          "name": random.choice(NpcRole.weaponlist), 
          "strength": (roll_dice("d6") + roll_dice("d6")),
          "precision": (roll_dice("d6") + roll_dice("d6")),
          "damage": (roll_dice("d6") + roll_dice("d6")),
          "toughness": (roll_dice("d6") + roll_dice("d6"))
    } )

    def __post_init__(self):
        self._vitality = self.vitality

## coins
## 100 iron = bronze
## 100 bronze = electrum
## 100 electrum = paladium
## 1000 paladium = crypto

@dataclass
class NpcRole:
    archetype: tuple=("hitter","hacker","grifter","thief","ranger","hunter",
                      "survivalist","el mariachi","cleric","shaman","dwiw","furian","mr punchy")
    weaponlist: tuple=("left thumb", "fountain pen", "frying pan", "lead pipe", "wrench", 
                    "the pacifier", "garlic spit", "unloaded gun handle",
                    "cheese grater", "knuckle busters", "bataan", "rolled up parchment",
                    "stink eye", "a good scratchin", "og titty twister")
    
@dataclass
class Loot_XP:
    xpneed: int = 0
    xphave: int = 0
    xplvl: int = 0
    lootmax: int = 30
## main program
