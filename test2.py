import threading
import queue
import random
import time

def producer(q, name1, name2):
    for i in range(5):
        q.put((name1, name2))  # Use tuple instead of set
        print(f"producer sent - name1: {name1}      name2: {name2}")
        time.sleep(random.random())

def consumer(q):
    while True:
        name1, name2 = q.get()
        print(f"Consumer received - name1: {name1}, name2: {name2}")
        q.task_done()

# Create a queue
q = queue.Queue()

# Define some example names
first_name = "John"
middle_name = "Doe"

# Create producer and consumer threads
prod1_thread = threading.Thread(target=producer, args=(q, first_name, middle_name))
prod2_thread = threading.Thread(target=producer, args=(q, middle_name, first_name))
consumer_thread = threading.Thread(target=consumer, args=(q,))

# Start the threads
prod1_thread.start()
prod2_thread.start()
consumer_thread.start()

# Join the producer threads
prod1_thread.join()
prod2_thread.join()

# Block until all items in the queue have been processed
q.join()

# Since the consumer thread is running in an infinite loop, it will continue indefinitely unless stopped
