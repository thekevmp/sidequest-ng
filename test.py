# Import the necessary modules.
import os
import sys
import random
import time
import curses
#from curses import wrapper
#from curses.textpad import Textbox, rectangle
#from sq_quests import *
from sq_npc_variables import *
from sq_mob_variables import *
#from sq_mob_variables import *
#from sq_weapons_coins_loot_variables import *
#from sq_combat_variables import *
from sq_rollfunction import *
from sq_progressbar import progressbar
#from sq_dealxp import dealxp
#from sq_journalloot import *
from sq_npcvmob_new import npcvmob_new
import queue
import threading
random.seed(time.time())
SENTINEL = object() # Sentinel value to signal the consumer to exit


# Define a function for the producer
def producer(q, attack, defend, logfile):
    while ((attack.vitality > 0) and (defend.vitality > 0)):
        q.put((attack, defend, roll_dice("d20"), logfile))
        print(f"producer sent - offense: {attack.name}      defense: {defend.name}")
        time.sleep(random.random())
    #q.put(SENTINEL)

# Define a function for the consumer
def consumer(q):
    while True:
        combatants = q.get()
        attack, defend, attack_roll, log = combatants
        if (attack_roll + attack.focus) > (defend.toughness + defend.focus):
            attack.weapon["toughness"] -= .25
            if attack.weapon["toughness"] <= 0:
                rollweapon()
            defend.vitality -= int(attack.weapon["strength"] * attack.strength / attack.toughness)
         #   defend.vitality -= int(2 * attack.strength / attack.toughness)
        else:
            #missed attack
            flight=1

        if npc.vitality <= 0:
            npc.vitality == (npc._vitality + npc.healing)
            #  sqnnline=[npc_stats["name"], "gets an", "adrenaline boost"]

        print(f"consumer received - offense: {attack.name}: {attack.vitality} attack roll: {attack_roll}     defense: {defend.name}: {defend.vitality}", file=log)
        q.task_done()

# Create a message queue
q = queue.Queue()


npc=NpcStats(name="handsomedrave")
npc_deux=NpcStats(name="el malitol")
mob1=MobStats()
mob2=MobStats()

npc_log = open("npc_sq.log", "w")
npc_deux_log = open("npc_duex_sq.log", "w")
npc_log.write("sidequest start\n")
npc_deux_log.write("sidequest start\n")

print(npc, file=npc_log)
print(mob1, file=npc_log)
print(npc_deux, file=npc_deux_log)
print(mob2, file=npc_deux_log)
#npc_log.write(str(npc))
#npc_log.write(str(mob1))
#npc_log.write(str(npc_deux))
#npc_deux_log.write(str(mob2))


print(npc)
print("")
print(mob1)
print("")
print(npc_deux)
print("")
print(mob2)



# Create and start the producer and consumer threads
npc_thread = threading.Thread(target=producer, args=(q, npc, mob1, npc_log))
mob_thread = threading.Thread(target=producer, args=(q, mob1, npc, npc_log))
npc_deux_thread = threading.Thread(target=producer, args=(q, npc_deux, mob2, npc_deux_log))
mob2_thread = threading.Thread(target=producer, args=(q, mob2, npc_deux, npc_deux_log))
consumer_thread = threading.Thread(target=consumer, args=(q,))


npc_thread.start()
consumer_thread.start()
mob_thread.start()
npc_deux_thread.start()
mob2_thread.start()

# Wait for the threads to finish
npc_thread.join()
mob_thread.join()
npc_deux_thread.join()
mob2_thread.join()

q.join()


#reset the queue
with q.mutex:
    q.queue.clear()

npc_log.write("sidequest end\n")
npc_log.close()
npc_deux_log.write("sidequest end\n")
npc_deux_log.close()
