def uisetup(tmpscr):
  global npc_win
  global loot_winh
  global loot_winf
  global coin_win
  global sqnn_win
  global journal_winh
  global loot_pad
  global journal_pad
  global stdscr
  stdscr = tmpscr

  #<window> = curses.newwin(h, w, r, c)
  #<pad> = curses.newpad(h,w)
  #<pad>.refresh(pr, pc, r, c, h, w)
  #<pad>.refresh(pad content-r, pad content-c, display screen row, display screen column, height, width)
  #start pad at cr, cc.
  
  stdscr.clear()
  rectangle(stdscr, 0, 0, 22, 79)       #main rectangle
  rectangle(stdscr, 0,0,15,21)          #npc stats
  rectangle(stdscr, 15,0,22,21)         #sidequest news network
  rectangle(stdscr, 0,21,18,50)         #loot
  rectangle(stdscr, 0,50,18,79)         #journal window
  stdscr.refresh()

  npc_win = curses.newwin(14, 20, 1, 1)   #npc stats window
  coin_win = curses.newwin(6, 20, 16 ,1)
  loot_winh = curses.newwin(1,28, 1, 22)
  loot_winf = curses.newwin(1,28, 17,22)
  journal_winh = curses.newwin(1,27,1,51)
  sqnn_win = curses.newwin(3, 56, 19, 22)

  loot_pad = curses.newpad(400,30)        #loot area
  journal_pad = curses.newpad(1200,27)

  npc_win.clear()
  coin_win.clear()
  loot_pad.clear()
  loot_winh.clear()
  loot_winf.clear()
  journal_winh.clear()
  journal_pad.clear()
  sqnn_win.clear()

  loot_winh.refresh()
  loot_winf.refresh()
  loot_pad.refresh(0,0,2,22,16,45)
  journal_pad.refresh(0,0,2,51,16,78)
  sqnn_win.refresh()

  time.sleep(timeless)

# define npc ui layout
def npcui():
  global xpneed
  global xphave
  global xplvl

  npc_win.refresh()
  time.sleep(timeless)
  npc_win.addstr(0,0, "npc:".center(20," "))
  winrow=1
  for k in npc_stats.keys():
    if k == "name" or k == "archetype":
      npc_win.addstr(winrow,0,(str(npc_stats[k])))
      npc_win.addstr(1,13,(str(npc_stats["level"]).rjust(7," ")))
      npc_win.refresh()
    elif k != "level" and k[0:1] != "_":
      if k == "xp":
        winrow +=0
      npc_win.addstr(winrow,0,(k + ": "))
      npc_win.addstr(winrow,13,(str(npc_stats[k]).rjust(7," ")))
      npc_win.refresh()
    winrow += 1
  #totalxp = str(npc_stats["xp"]) + "-" + str(xplvl)
  totalxp = str(npc_stats["xp"] - xphave) + "/" + str(xpneed)
  #totalxp = str(npc_stats["xp"] - xphave) + "/" + str(xpneed)
  npc_win.addstr(12,5,(totalxp.rjust(15," ")))  
  npc_win.refresh()
  time.sleep(timeless)

def sqnnui(displayitems):
  sqnn_win.clear()
  sqnn_win.addstr(0,0,"sidequest news network".center(58," "))
#  line=2
  sqnn_txt = ""
  for linestr in displayitems:
    sqnn_txt += str(linestr[0:18]) + " "
    #sqnn_win.addstr(line,0, str(linestr[0:18]))
#    line +=1
  sqnn_win.addstr(1,0,sqnn_txt)
  sqnn_win.refresh()
  return

def coinbagui():
  coin_win.clear()
  coin_win.addstr(0,0,"coin bag".center(20," "))
  line=1
  for coin in npc_coins.keys():
    coin_win.addstr(line,0, coin + ": ")
    coin_win.addstr(line, 11, str(npc_coins[coin]).rjust(8," "))
    line +=1
  coin_win.refresh()
  return

def lootui():
  tmpbar=""
  loot_winh.insstr(0,0,"loot:".center(28," "))
  loot_winh.refresh()
  winrow=0
  if len(npc_loot) > 16:
    winrow = (len(npc_loot) - 16)
  for lootcnt in range(len(npc_loot)):
    tmploot = npc_loot[lootcnt]
    tmploot = tmploot.ljust(30," ")
    loot_pad.addstr(lootcnt,0,tmploot[0:25])
    loot_pad.refresh(winrow,0,2,22,16,45)
    time.sleep(timeless)
  tmpbar=progressbar(len(npc_loot), 30, 1, 28)
  loot_winf.insstr(0,0,tmpbar)
  loot_winf.refresh()


def journalui():
  journal_winh.clear()
  journal_winh.insstr(0,7,"journal: ")
  journal_winh.refresh()
  #journal_pad.refresh(0,0,2,51,16,78)
  wrow=0
  for journalcnt in range(len(npc_journal)-1,-1,-1):
    if journalcnt != len(npc_journal)-1:
      journal_pad.addstr(wrow,0, "\u2713")
    tmpjournal = npc_journal[journalcnt]
    tmpjournal = tmpjournal.ljust(81," ")
    journal_pad.addstr(wrow,1, tmpjournal)
    wrow += 3
    journal_winh.addstr(0,17,(str(len(npc_journal)).rjust(4," ")))
    journal_pad.refresh(0,0,2,51,16,78)
    journal_winh.refresh()
  time.sleep(timeless)
